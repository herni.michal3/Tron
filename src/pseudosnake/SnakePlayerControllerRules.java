package pseudosnake;


import engine.controller.rules.GenericPlayerControllerRules;
import engine.model.Direction;
import engine.controller.GenericPlayerController;

public class SnakePlayerControllerRules extends GenericPlayerControllerRules {

    public SnakePlayerControllerRules(GenericPlayerController controller) {
        super(controller);
        addBlockedMoves();
    }

    /**
     *
     * Mapping of blocked moves depending on actual orientation <br>
     * First Direction: current direction <br>
     * Second Direction: blocked direction <br>

     *
     */
    private void addBlockedMoves() {
        blockedActions.put(Direction.UP, Direction.DOWN);
        blockedActions.put(Direction.DOWN, Direction.UP);
        blockedActions.put(Direction.LEFT, Direction.RIGHT);
        blockedActions.put(Direction.RIGHT, Direction.LEFT);
    }

    @Override
    protected void moveAction(Direction direction) {
        if (!blockedActions.get(direction).equals(controller.getPlayer().getDirection())) {
        controller.getActuator().changeDirection(direction);
        controller.getActuator().controlledMove();
        }
    }

}
