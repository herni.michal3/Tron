package pseudosnake;


import engine.controller.MenuController;
import engine.Core;
import engine.model.Bounds;
import engine.model.Direction;
import engine.controller.GenericPlayerController;
import engine.model.DefaultKeyboardMapping;
import engine.model.DefaultMouseMapping;
import engine.model.GameObject;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.KeyListener;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelListener;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author xtravni2
 */
public class SnakeGame extends Core {

    private final List<GenericPlayerController> controllers = new ArrayList<>();
    private final List<SnakeMover> playerActuators = new ArrayList<>();

   private Bounds screenBounds;

    @Override
    public void init() {
        super.init();
        
        setCollisionProcessor(new SnakeCollisionProcessor());
                        
        screenBounds = new Bounds(new Point(0, 0), new Point(getRenderer().getWidth(), getRenderer().getHeight()));

        
        addGameObject(new SnakePlayer(new Point(40, 40), Direction.RIGHT, Color.GREEN, screenBounds));
       // players.add(new SnakePlayer(new Point(400, 45), Direction.LEFT, Color.BLUE, screenBounds));
        addGameObject(new SnakePlayer(new Point(400, 440), Direction.LEFT, Color.RED, screenBounds));
        
         addGameObject(new GameObject(new Point(400, 140), Direction.LEFT, Color.WHITE, screenBounds));
         addGameObject(new GameObject(new Point(250, 240), Direction.LEFT, Color.WHITE, screenBounds));
         addGameObject(new GameObject(new Point(300, 240), Direction.LEFT, Color.WHITE, screenBounds));
         addGameObject(new GameObject(new Point(300, 440), Direction.LEFT, Color.WHITE, screenBounds));
         addGameObject(new GameObject(new Point(400, 240), Direction.LEFT, Color.WHITE, screenBounds));
         addGameObject(new GameObject(new Point(300, 340), Direction.LEFT, Color.WHITE, screenBounds));
         
         
         
//          addGameObject(new SnakePlayer(new Point(400, 440), Direction.LEFT, Color.RED, screenBounds));
//           addGameObject(new SnakePlayer(new Point(400, 440), Direction.LEFT, Color.RED, screenBounds));
//        
        
        //players.add(new SnakePlayer(new Point(400, 240), Direction.LEFT, Color.BLUE));
        //players.add(new SnakePlayer(new Point(490, 490), Direction.UP, Color.WHITE));
        //players.add(new SnakePlayer(new Point(10, 10), Direction.DOWN, Color.ORANGE));
                
        Component window = (Component) getRenderer().getWindow();
        
        for (GameObject object : getGameObjects()) {
            
           if (object.getClass().equals(SnakePlayer.class)) {
            playerActuators.add(new SnakeMover((SnakePlayer) object));
           }
            
        }
        
        controllers.add(new SnakeKeyboardController((SnakePlayer) getGameObjects().get(0),playerActuators.get(0)));
        controllers.add(new SnakeMouseController((SnakePlayer) getGameObjects().get(1),playerActuators.get(1)));
        MenuController menu = new MenuController(this);

        controllers.get(0).setDefaultMapping(DefaultKeyboardMapping.ONE);
        controllers.get(1).setDefaultMapping(DefaultMouseMapping.ONE);

        window.addKeyListener((KeyListener) menu.getListener());
        window.addKeyListener((KeyListener) controllers.get(0).getListener());
        window.addMouseListener((MouseListener) controllers.get(1).getListener());
        window.addMouseMotionListener((MouseMotionListener) controllers.get(1).getListener());
        window.addMouseWheelListener((MouseWheelListener) controllers.get(1).getListener());
        
        

    }

    @Override
    public void update() {     
        playerActuators.forEach(SnakeMover::collisionAwareMove);
        if (getGameObjects().isEmpty()) {
            gameOver();
        }
    }

    private void gameOver() {
        
        System.out.println("Game Over");
        stop();
    }

    @Override
    public <T> void draw(T g) {
        drawBackround((Graphics2D) g);
        drawBackround((Graphics2D) getRenderer().getGraphics());
        for (GameObject object : getGameObjects()) {
            drawGameObject(object, (Graphics2D) g);
        }      
    }

    private void drawGameObject(GameObject object, Graphics2D g) {
        for (int x = 0; x < object.getLength(); x++) {
            Point point = object.getBodySegment(x);
            
            g.setColor(object.getColor());
            g.fillRect(point.x - object.getSize() / 2, point.y - object.getSize() / 2,
                    object.getSize(), object.getSize());
        }
    }

    private void drawBackround(Graphics2D g) {
        g.setColor(Color.BLACK);
        g.fillRect(0, 0, renderer.getWidth(), renderer.getHeight());
    }


}
