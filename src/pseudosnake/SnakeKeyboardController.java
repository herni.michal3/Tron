package pseudosnake;

import engine.controller.GenericPlayerController;
import engine.controller.PlayerActuator;
import engine.controller.listener.ControllerKeyListener;

public class SnakeKeyboardController extends GenericPlayerController {

    public SnakeKeyboardController(SnakePlayer player,PlayerActuator actuator) {
        super(player,actuator);
        setControllerRules(new SnakePlayerControllerRules(this));
        setControllerListener(new ControllerKeyListener(this));

    }

}
