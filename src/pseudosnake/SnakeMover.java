package pseudosnake;

import engine.Core;
import engine.GenericMover;
import engine.controller.PlayerActuator;
import engine.model.Direction;

/**
 *
 * @author Michal
 */
public class SnakeMover extends GenericMover implements PlayerActuator {

    private final SnakePlayer player;

    public SnakeMover(SnakePlayer player) {
        super(player);
        this.player = player;
    }

    @Override
    protected void boundsNotHit() {
        //TODO chybi jeden trail na zacatku
        player.addTrail(player.getLocation());
        if(player.getPath().size()>player.getElongation()+20) {
            player.getPath().removeLast();
        }        
    }

    @Override
    protected void boundsHit() {
        
    }

    @Override
    public boolean checkCollisionsAhead(Direction direction) {
        return Core.getCore().getCollisionProcessor().checkCollisionAhead(player, direction);

    }
   
    @Override
    public void onCollision() {
        System.out.println("Collision");
        
    }
    
    @Override
    public void start() {
    }

    @Override
    public void stop() {
    }

    @Override
    public void changeDirection(Direction direction) {
        player.setDirection(direction); 
    }

  
    @Override
    public void controlledMove() {        
        collisionAwareMove();               
    }

}
