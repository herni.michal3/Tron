package pseudosnake;

import engine.model.Player;
import engine.model.Bounds;
import engine.model.Direction;
import java.awt.Color;
import java.awt.Point;
import java.util.LinkedList;

/**
 *
 * @author Michal
 */
public class SnakePlayer extends Player {

    private final LinkedList<Point> path = new LinkedList<>();
    
    private int elongation = 0;

    public SnakePlayer(Point location, Direction direction, Color color, Bounds bounds) {
        super(location, direction, color, bounds);       
        addTrail(location);
        setBounds(bounds);
    }
    
    public int getElongation() {
        return elongation;
    }
    
    public void setElongation(int num) {
        elongation = num;
    }
    
    public void addElongation(int num) {
        elongation += num;
    }
    
    @Override
    public Point getLocation() {
        return path.get(0);
    }
        
    @Override
    public int getLength() {
        return path.size();
    }
    
    public LinkedList getPath() {
        return path;
    }
        
    @Override
    public Point getBodySegment(int x) {
        return path.get(x);
    }

    public final void addTrail(Point point) {
        path.addFirst(new Point(point));
    }

}
