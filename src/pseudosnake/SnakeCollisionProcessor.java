package pseudosnake;

import engine.controller.CollisionProcessor;
import engine.model.GameObject;

/**
 *
 * @author Michal
 */
public class SnakeCollisionProcessor extends CollisionProcessor {

    
    @Override
    public <T extends GameObject> void collisionHandling(T collider, T collidedWith) {
       
            getGameObjects().remove(collidedWith);
            SnakePlayer snake = (SnakePlayer) collider;
            
           if (collidedWith.getClass().equals(SnakePlayer.class)) {
             System.out.println("Got him");
             snake.addElongation(collidedWith.getLength());
           } 
           else {
               snake.addElongation(5);
           }
             

    }

    


}
