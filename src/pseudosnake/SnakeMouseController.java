package pseudosnake;

import engine.controller.GenericPlayerController;
import engine.controller.PlayerActuator;
import engine.controller.listener.ControllerMouseListener;

public class SnakeMouseController extends GenericPlayerController {

    private SnakePlayer player;

    public SnakeMouseController(SnakePlayer player, PlayerActuator actuator) {
        super(player,actuator);
        this.player = player;
        setControllerRules(new SnakePlayerControllerRules(this));
        setControllerListener(new ControllerMouseListener(this));

    }


}
