package tron;

import engine.model.Player;
import engine.model.Bounds;
import engine.model.Direction;
import java.awt.Color;
import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Michal
 */
public class TronPlayer extends Player {

    private final List<Point> path = new ArrayList<>();    

    public TronPlayer(Point location, Direction direction, Color color, Bounds bounds) {
        super(location, direction, color, bounds);       
        addTrail(location);
        setBounds(bounds);
    }
    
    @Override
    public Point getLocation() {
        return path.get(0);
    }
        
    @Override
    public int getLength() {
        return path.size();
    }
        
    @Override
    public Point getBodySegment(int x) {
        return path.get(x);
    }

    protected final void addTrail(Point point) {
        path.add(new Point(point));
    }

}
