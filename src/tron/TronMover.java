package tron;

import engine.GenericMover;
import engine.controller.PlayerActuator;
import engine.model.Direction;

/**
 *
 * @author Michal
 */
public class TronMover extends GenericMover implements PlayerActuator {

    private final TronPlayer player;

    public TronMover(TronPlayer player) {
        super(player);
        this.player = player;
    }

    @Override
    public boolean checkBounds() {
        return canMoveInBounds();
    }

    @Override
    protected void boundsNotHit() {
        
        player.addTrail(player.getLocation());

    }

    @Override
    protected void boundsHit() {
        objectAroundBounds(object.getDirection(), object.getBounds());
    }

    @Override
    public void onCollision() {
        System.out.println("Collision");

    }

    @Override
    public void start() {
    }

    @Override
    public void stop() {
    }

    @Override
    public void changeDirection(Direction direction) {
        if (canMoveInBounds()) {
            object.setDirection(direction);
        }
    }

    @Override
    public void controlledMove() {

    }

}
