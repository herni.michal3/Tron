package tron;

import engine.controller.MenuController;
import engine.Core;
import engine.GenericMover;
import engine.model.Bounds;
import engine.model.Direction;
import engine.controller.GenericPlayerController;
import engine.model.DefaultKeyboardMapping;
import engine.model.DefaultMouseMapping;
import engine.model.GameObject;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.KeyListener;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelListener;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author xtravni2
 */
public class TronGame extends Core {

    private final List<GenericPlayerController> controllers = new ArrayList<>();
    private final List<TronMover> playerActuators = new ArrayList<>();

   private Bounds screenBounds;

    @Override
    public void init() {
        super.init();
        
        setCollisionProcessor(new TronCollisionProcessor());
                        
        screenBounds = new Bounds(new Point(0, 0), new Point(getRenderer().getWidth(), getRenderer().getHeight()));

        
        addGameObject(new TronPlayer(new Point(40, 40), Direction.RIGHT, Color.GREEN, screenBounds));
       // players.add(new TronPlayer(new Point(400, 45), Direction.LEFT, Color.BLUE, screenBounds));
        addGameObject(new TronPlayer(new Point(400, 440), Direction.LEFT, Color.RED, screenBounds));
                
        Component window = (Component) getRenderer().getWindow();
        
        for (GameObject player : getGameObjects()) {
            playerActuators.add(new TronMover((TronPlayer) player));            
        }
        
        controllers.add(new TronKeyboardController((TronPlayer) getGameObjects().get(0),playerActuators.get(0)));
        controllers.add(new TronMouseController((TronPlayer) getGameObjects().get(1),playerActuators.get(1)));
        MenuController menu = new MenuController(this);

        controllers.get(0).setDefaultMapping(DefaultKeyboardMapping.ONE);
        controllers.get(1).setDefaultMapping(DefaultMouseMapping.ONE);

        window.addKeyListener((KeyListener) menu.getListener());
        window.addKeyListener((KeyListener) controllers.get(0).getListener());
        window.addMouseListener((MouseListener) controllers.get(1).getListener());
        window.addMouseMotionListener((MouseMotionListener) controllers.get(1).getListener());
        window.addMouseWheelListener((MouseWheelListener) controllers.get(1).getListener());
        
        

    }

    @Override
    public void update() {     
        playerActuators.forEach(GenericMover::collisionAwareMove);       
    }

    private void gameOver() {
        System.out.println("Game Over");
        stop();
    }

    @Override
    public <T> void draw(T g) {
        drawBackround((Graphics2D) g);
        drawBackround((Graphics2D) getRenderer().getGraphics());
        for (GameObject object : getGameObjects()) {
            drawGameObject(object, (Graphics2D) g);
        }      
    }

    private void drawGameObject(GameObject object, Graphics2D g) {
        for (int x = 0; x < object.getLength(); x++) {
            Point point = object.getBodySegment(x);
            
            g.setColor(object.getColor());
            g.fillRect(point.x - object.getSize() / 2, point.y - object.getSize() / 2,
                    object.getSize(), object.getSize());
        }
    }

    private void drawBackround(Graphics2D g) {
        g.setColor(Color.BLACK);
        g.fillRect(0, 0, renderer.getWidth(), renderer.getHeight());
    }


}
