package tron;

import engine.controller.GenericPlayerController;
import engine.controller.PlayerActuator;
import engine.controller.listener.ControllerMouseListener;

public class TronMouseController extends GenericPlayerController {

    private TronPlayer player;

    public TronMouseController(TronPlayer player, PlayerActuator actuator) {
        super(player,actuator);
        this.player = player;
        setControllerRules(new TronPlayerControllerRules(this));
        setControllerListener(new ControllerMouseListener(this));

    }


}
