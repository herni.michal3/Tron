package tron;

import engine.Core;
import engine.controller.CollisionProcessor;
import engine.model.GameObject;


/**
 *
 * @author Michal
 */
public class TronCollisionProcessor extends CollisionProcessor {
    
    @Override
    public void collisionHandling(GameObject collider, GameObject collidedWith) {
     
        Core.getCore().stop();
    }
        
    

}
