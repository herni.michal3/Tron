package tron;

import engine.controller.GenericPlayerController;
import engine.controller.PlayerActuator;
import engine.controller.listener.ControllerKeyListener;

public class TronKeyboardController extends GenericPlayerController {

    private TronPlayer player;

    public TronKeyboardController(TronPlayer player,PlayerActuator actuator) {
        super(player,actuator);
        this.player = player;
        setControllerRules(new TronPlayerControllerRules(this));
        setControllerListener(new ControllerKeyListener(this));


    }

}
