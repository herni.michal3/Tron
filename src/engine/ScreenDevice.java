
package engine;

import java.awt.DisplayMode;
import java.awt.Graphics2D;
//import java.awt.Window;

/**
 *
 * @author Michal
 */
public interface ScreenDevice<T,A> {
    
    void closeScreen();

    T getGraphics();

    int getHeight();

    int getWidth();

    A getWindow();

    void init();

    //void initScreen(DisplayMode displayMode);

    void update();

}
