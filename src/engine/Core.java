package engine;

import engine.controller.CollisionProcessor;
import engine.model.GameObject;
import java.util.ArrayList;
import java.util.List;

public abstract class Core {

    private boolean running;
    protected Renderer renderer;
    final int SLEEP_DELAY = 100;
    private final List<GameObject> gameObjects = new ArrayList<>();
    private CollisionProcessor collisionProcessor;
    private static Core instance;

    public void stop() {
        setRunning(false);
    }

    public void run() {
        try {
            init();
            gameLoop();
        } finally {
            renderer.closeScreen();
        }
    }

    public void init() {
        renderer = new Renderer();
        renderer.init();
        instance = this;
        setRunning(true);
    }

    public abstract void update();

    public abstract <T> void draw(T g);

    protected boolean isRunning() {
        return running;
    }
    
    public static Core getCore(){
        return instance;
    }
    
    public Renderer getRenderer(){
           return renderer;
    }
    

    protected void setRunning(boolean running) {
        this.running = running;
    }

    private void gameLoop() {
        while (isRunning()) {
            draw(renderer.getGraphics());
            renderer.update();
            update();
            try {
                Thread.sleep(SLEEP_DELAY);
            } catch (Exception ex) {
            }
        }
    }
    
    public List<GameObject> getGameObjects() {
        return gameObjects;        
    }
    
    public void addGameObject(GameObject object) {
        gameObjects.add(object);
    }
    
    public void addGameObjects(List<? extends GameObject> objects) {
        gameObjects.addAll(objects);
    }

    public CollisionProcessor getCollisionProcessor() {
        return collisionProcessor;
    }

    public void setCollisionProcessor(CollisionProcessor collisionProcessor) {
        this.collisionProcessor = collisionProcessor;
    }
    
}
