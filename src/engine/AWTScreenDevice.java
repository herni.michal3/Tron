package engine;

import java.awt.Dimension;
import java.awt.DisplayMode;
import java.awt.Graphics2D;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.image.BufferStrategy;
import javax.swing.JFrame;


/**
 *
 * @author Michal
 */
public class AWTScreenDevice implements ScreenDevice{

    static final DisplayMode[] modes = {
        new DisplayMode(1680, 1050, 32, 0),
        new DisplayMode(800, 600, 32, 0),
        new DisplayMode(800, 600, 24, 0),
        new DisplayMode(800, 600, 16, 0),
        new DisplayMode(640, 480, 32, 0),
        new DisplayMode(640, 480, 24, 0),
        new DisplayMode(640, 480, 16, 0)};

    private final GraphicsDevice graphicsDevice;
    private Window window;
    private final boolean fullScreenMode = false;

    public AWTScreenDevice() {
        GraphicsEnvironment e = GraphicsEnvironment.getLocalGraphicsEnvironment();
        graphicsDevice = e.getDefaultScreenDevice();
    }

    @Override
    public void init() {
        DisplayMode dm = findFirstCompatibleMode(AWTScreenDevice.modes);
        initScreen(dm);
    }

    public DisplayMode[] getCompatibleDisplayModes() {
        return graphicsDevice.getDisplayModes();
    }

    public DisplayMode findFirstCompatibleMode(DisplayMode[] modes) {

        DisplayMode goodModes[] = graphicsDevice.getDisplayModes();
        for (int x = 0; x < modes.length; x++) {
            for (int y = 0; y < goodModes.length; y++) {
                if (displayModesMatch(modes[x], goodModes[y])) {
                    return modes[x];
                }
            }
        }
        return null;
    }

    public boolean displayModesMatch(DisplayMode mode1, DisplayMode mode2) {
        if (mode1.getWidth() != mode2.getWidth()
                || mode1.getHeight() != mode2.getHeight()) {
            return false;
        }
        if (mode1.getBitDepth() != DisplayMode.BIT_DEPTH_MULTI
                && mode2.getBitDepth() != DisplayMode.BIT_DEPTH_MULTI
                && mode1.getBitDepth() != mode2.getBitDepth()) {
            return false;
        }
        if (mode1.getRefreshRate() != DisplayMode.REFRESH_RATE_UNKNOWN
                && mode2.getRefreshRate() != DisplayMode.REFRESH_RATE_UNKNOWN
                && mode1.getRefreshRate() != mode2.getRefreshRate()) {
            return false;
        }
        return true;
    }

    public void initScreen(DisplayMode displayMode) {
        JFrame window = new JFrame();
        window.setUndecorated(true);
        window.setIgnoreRepaint(true);
        window.setResizable(false);

        if (fullScreenMode) {
            graphicsDevice.setFullScreenWindow(window);

            if (displayMode != null && graphicsDevice.isDisplayChangeSupported()) {
                try {
                    graphicsDevice.setDisplayMode(displayMode);
                } catch (Exception ex) {
                }
                window.createBufferStrategy(2);
            }

        } else {

            Dimension screenDimension = Toolkit.getDefaultToolkit().getScreenSize();
            final int windowSize = 500;
            window.setSize(windowSize, windowSize);
            window.setLocation(screenDimension.width / 2 - window.getSize().width / 2,
                    screenDimension.height / 2 - window.getSize().height / 2);
            window.setVisible(true);
            window.createBufferStrategy(2);
            System.out.println(window.getBufferStrategy());
        }
        this.window = window;

    }

    public Graphics2D getGraphics() {
        Window w = getWindow();
        if (w != null) {
            BufferStrategy bs = w.getBufferStrategy();
            return (Graphics2D) bs.getDrawGraphics();
        } else {
            return null;
        }
    }

    @Override
    public void update() {
        Window w = getWindow();
        if (w != null) {
            BufferStrategy bs = w.getBufferStrategy();
            if (!bs.contentsLost()) {
                bs.show();
            }
        }
    }

    @Override
    public int getWidth() {
        Window w = getWindow();
        if (w != null) {
            return w.getWidth();
        } else {
            return 0;
        }
    }

    public Window getWindow() {
        return window;
    }

    @Override
    public int getHeight() {
        Window w = getWindow();
        if (w != null) {
            return w.getHeight();
        } else {
            return 0;
        }
    }

    @Override
    public void closeScreen() {
        Window w = getWindow();
        if (w != null) {
            w.dispose();
        }
        graphicsDevice.setFullScreenWindow(null);
    }

   

}
