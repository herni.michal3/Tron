
package engine;

import engine.model.Bounds;
import engine.model.Screen;
import java.awt.Point;


/**
 *
 * @author Michal
 */
public class Renderer {
    
    private Screen screen;

    public Renderer() {
        screen = new Screen(Screen.ScreenMode.WINDOW, new AWTScreenDevice());
        screen.setBounds(new Bounds(new Point(0, 0), new Point(getWidth(), getHeight())));        
    }
        
    void closeScreen() {
        getScreen().getDevice().closeScreen();
    }

    void init() {
        getScreen().getDevice().init();
    }
    
    public Object getGraphics(){
        return getScreen().getDevice().getGraphics();
    }

    void update() {        
        getScreen().getDevice().update();
    }

    public Object getWindow() {
        return getScreen().getDevice().getWindow();
    }
    
    public int getHeight() {
        return getScreen().getDevice().getHeight();
    }

    public int getWidth(){
        return getScreen().getDevice().getWidth();        
    }

    public Screen getScreen() {
        return screen;
    }

    public void setScreen(Screen screen) {
        this.screen = screen;
    }

}
