package engine;

import engine.model.Bounds;
import engine.model.Direction;
import engine.model.GameObject;
import java.awt.Point;

/**
 *
 * @author Michal
 */
public abstract class GenericMover {

    protected GameObject object;

    public GenericMover(GameObject object) {
        this.object = object;
    }
    protected abstract void boundsHit();

    protected abstract void boundsNotHit();

    public abstract void onCollision();
    
 
    public boolean checkBounds() {
        return canMoveInBounds();
    }
    
    
    public void collisionAwareMove() {
        if (checkCollisionsAhead(object.getDirection())) {
            onCollision();
        }
        if (checkBounds()) {
            directMove();
            boundsNotHit();
        } else {
            boundsHit();
        }
    }
    
    public boolean checkCollisionsAhead(Direction direction) {
        return Core.getCore().getCollisionProcessor().checkCollisionAhead(object, direction);

    }
    

    public boolean canMoveInBounds() {
        return tryMoveInBounds(object.getMoveStep(), object.getDirection(), object.getBounds());
    }

    public boolean canMoveInBounds(Direction direction) {
        return tryMoveInBounds(object.getMoveStep(), direction, object.getBounds());
    }

    public boolean tryMoveInBounds(int step, Direction direction, Bounds bounds) {
        switch (direction) {
            case UP:
                return object.getLocation().y - step > bounds.getStartY();
            case RIGHT:
                return object.getLocation().x + step < bounds.getEndX();
            case DOWN:
                return object.getLocation().y + step < bounds.getEndY();
            case LEFT:
                return object.getLocation().x - step > bounds.getStartX();
        }
        return false;
    }

    public void objectAroundBounds(Direction direction, Bounds bounds) {

        Point point = object.getLocation();
        switch (direction) {
            case UP:
                point.setLocation(point.x, bounds.getEndY());
                break;
            case RIGHT:
                point.setLocation(bounds.getStartX(), point.y);
                break;
            case DOWN:
                point.setLocation(point.x, bounds.getStartY());
                break;
            case LEFT:
                point.setLocation(bounds.getEndX(), point.y);
                break;
        }
    }

//    public void objectAroundBounds(Bounds bounds){
//        pointAroundBounds(object.getLocation(), object.getDirection(), bounds);
//    }
    public void directMove() {
        translatePoint(object.getLocation(), object.getDirection(), object.getMoveStep());
    }

    /**
     * Moves point from current location by <b>step</b>
     *
     * @param point
     * @param direction
     * @param step
     */
    public void translatePoint(Point point, Direction direction, int step) {
        Point vector = direction.getVector();
        point.translate(vector.x * step, vector.y * step);
    }

}
