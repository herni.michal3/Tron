package engine.controller;

import engine.model.GameObject;

/**
 *
 * @author Michal
 */
public interface CollisionHandling {

    <T extends GameObject> void collisionHandling(T collider, T collision);
    
}
