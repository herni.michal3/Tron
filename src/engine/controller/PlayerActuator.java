package engine.controller;

import engine.model.Direction;

/**
 *
 * @author Michal
 */
public interface PlayerActuator {

    /**
     * Move initiated by controller
     *
     */
    void controlledMove();

    void start();

    void stop();

    void changeDirection(Direction direction);

}
