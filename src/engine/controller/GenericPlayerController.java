package engine.controller;

import engine.model.Player;

public class GenericPlayerController extends BasicController {

    private Player player;
    private PlayerActuator actuator;

    public PlayerActuator getActuator() {
        return actuator;
    }

    public GenericPlayerController(Player player, PlayerActuator actuator) {
        this.player = player;
        this.actuator = actuator;
    }

    public Player getPlayer() {
        return player;
    }

}
