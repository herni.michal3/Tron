package engine.controller;

import engine.Core;
import engine.controller.listener.ControllerKeyListener;
import engine.controller.rules.MenuRules;
import engine.model.MenuAction;
import java.awt.event.KeyEvent;

/**
 *
 * @author Michal
 */
public class MenuController extends BasicController {

    public MenuController(Core core) {

        setControllerListener(new ControllerKeyListener(this));
        setControllerRules(new MenuRules());
        
        getControllerBindings().put(KeyEvent.VK_1, new MenuAction());
        getControllerBindings().put(KeyEvent.VK_ESCAPE, () -> {
            core.stop();
            System.out.println("Exiting");
            System.exit(0);
        });

    }

}
