package engine.controller;

import engine.controller.listener.ControllerListener;
import engine.model.ControllerAction;
import engine.controller.rules.GenericControllerRules;
import engine.model.DefaultMapping;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 *
 * @author Michal
 */
public abstract class BasicController implements Controller{

    private GenericControllerRules controllerRules;
    private Map<Integer, ControllerAction> controllerBindings = new HashMap<>();
    private ControllerListener controllerListener;


    public Map<Integer, ControllerAction> getControllerBindings() {
        return controllerBindings;
    }
    
    @Override
    public void setControllerListener(ControllerListener controllerListener) {
        this.controllerListener = controllerListener;
    }

    @Override
    public void addControllerMapping(Integer mapping, ControllerAction action) {
        controllerBindings.put(mapping, action);
    }

    protected void setControllerRules(GenericControllerRules rules) {
        this.controllerRules = rules;
    }

    @Override
    public void canDoAction(Optional input) {
        if (input.isPresent()) {
            controllerRules.doAction((ControllerAction) input.get());
        }
    }

    @Override
    public Optional<ControllerAction> inputEvent(Integer e) {
        Optional mapping = returnControllerMapping(e);
        if (mapping.isPresent()) {
            return Optional.of((ControllerAction) mapping.get());
        } else {
            return Optional.empty();
        }
    }

    @Override
    public Optional<ControllerAction> returnControllerMapping(Integer i) {
        return Optional.ofNullable(controllerBindings.get(i));
    }

    @Override
    public ControllerListener getListener() {
        return controllerListener;
    }

    /**
     * @param controllerBindings the controllerBindings to set
     */
    protected void setControllerBindings(Map<Integer, ControllerAction> controllerBindings) {
        this.controllerBindings = controllerBindings;
    }

    /**
     * Default keyboard mappings. Either choose default or use addControllerMapping
     */
    public void setDefaultMapping(DefaultMapping mapping) {
        setControllerBindings(mapping.getMapping());
    }

}
