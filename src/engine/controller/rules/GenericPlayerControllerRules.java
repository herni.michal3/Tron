package engine.controller.rules;

import engine.model.ControllerAction;
import engine.controller.GenericPlayerController;
import engine.model.Direction;
import java.util.HashMap;
import java.util.Map;

public abstract class GenericPlayerControllerRules extends GenericControllerRules{

    protected GenericPlayerController controller;
    protected Map<ControllerAction, ControllerAction> blockedActions = new HashMap<>();

    public GenericPlayerControllerRules(GenericPlayerController controller) {
        this.controller = controller;
    }

    /**
     * Mapping of blocked actions <br>
     * First: controller input which is blocked <br>
     * Second: action which is in progress <br>
     *
     */
    public void bindBlockedActions(Map<ControllerAction, ControllerAction> blockedActions) {
        this.blockedActions = blockedActions;
    }

    public void doAction(ControllerAction input) {

        if (input.getClass().equals(Direction.class)) {
            moveAction((Direction) input);
        }

    }

    protected void moveAction(Direction direction) {

    }

}
