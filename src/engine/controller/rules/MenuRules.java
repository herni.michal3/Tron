/*
 * To change this template file, choose Tools | Templates
 */

package engine.controller.rules;

import engine.model.ControllerAction;

/**
 *
 * @author Michal
 */
public class MenuRules extends GenericControllerRules{

    public void doAction(ControllerAction controllerAction) {
        
        controllerAction.run();
        
    }

}
