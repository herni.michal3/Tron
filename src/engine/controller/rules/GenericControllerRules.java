/*
 * To change this template file, choose Tools | Templates
 */

package engine.controller.rules;

import engine.model.ControllerAction;

/**
 *
 * @author Michal
 */
public abstract class GenericControllerRules {

    public abstract void doAction(ControllerAction controllerAction);

}
