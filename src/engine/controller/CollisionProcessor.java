package engine.controller;

import engine.Core;
import engine.model.Direction;
import engine.model.GameObject;
import java.awt.Point;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Michal
 */
public abstract class CollisionProcessor implements CollisionHandling {

    public class Collision {

        protected GameObject collider;
        protected GameObject collidedWith;

        public Collision(GameObject collider, GameObject collidedWith) {
            this.collider = collider;
            this.collidedWith = collidedWith;
        }

        public GameObject getCollider() {
            return collider;
        }

        public GameObject getCollidedWith() {
            return collidedWith;
        }

    }
    
    protected Collection<GameObject> gameObjects;

    private final List<Collision> collisions = new ArrayList<>();

    public CollisionProcessor() {
        
        this.gameObjects = Core.getCore().getGameObjects();
    }


    public List<Collision> getCollisions() {
        return collisions;
    }

    public void clearCollisions() {
        collisions.clear();
    }
    
    protected Collection getGameObjects() {        
        return gameObjects;
    }

    public void setGameObjects(List<GameObject> gameObjects) {
        this.gameObjects = gameObjects;
    }
    
    /**
     * Handling of collisions between two gameObjects
     * @param <T>
     * @param collider
     * @param collidedWith
     */
    @Override
    public abstract <T extends GameObject> void collisionHandling(T collider, T collidedWith);

    public <T extends GameObject> boolean checkCollisionAhead(T object, Direction direction) {
        Point p = object.getLocation();
        Point ahead = new Point(p.x + direction.getVector().x * object.getMoveStep(),
                p.y + direction.getVector().y * object.getMoveStep());
        return checkObjectCollision(object, ahead);
    }

    public boolean checkObjectCollision(GameObject mover, Point collider) {

        for (GameObject object : Core.getCore().getGameObjects()) {
            for (int i = 0; i < object.getLength(); i++) {
                Point body = object.getBodySegment(i);
                
                if (collider.equals(body) || !object.equals(mover) && collider.distance(body) < mover.getSize()) {

                    registerCollision(mover, object);
                    collisionHandling(mover, object);
                    return true;
                }
            }
        }
        return false;
    }

    public void registerCollision(GameObject collider, GameObject collidedWith) {
        getCollisions().add(new Collision(collider, collidedWith));

    }

}
