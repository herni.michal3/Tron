package engine.controller;

import engine.controller.listener.ControllerListener;
import engine.model.ControllerAction;
import java.util.Optional;

/**
 *
 * @author Michal
 */
public interface Controller {

    void addControllerMapping(Integer mapping, ControllerAction action);

    void canDoAction(Optional input);

    ControllerListener getListener();

    Optional<ControllerAction> inputEvent(Integer e);

    Optional<ControllerAction> returnControllerMapping(Integer i);

    void setControllerListener(ControllerListener controllerListener);
    
}
