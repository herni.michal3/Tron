package engine.controller.listener;

import engine.controller.BasicController;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.util.Optional;
import javax.swing.event.MouseInputListener;

/**
 *
 * @author Michal
 */
public class ControllerMouseListener extends ControllerListener implements  MouseInputListener,MouseWheelListener {
    
    BasicController controller;

    public ControllerMouseListener(BasicController controller) {
        this.controller = controller;
    }
    

@Override
    public void mouseWheelMoved(MouseWheelEvent e) {
        Optional input = controller.inputEvent(e.getID());
        controller.canDoAction(input);
    }

    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
        Optional input = controller.inputEvent(e.getButton());
        controller.canDoAction(input);

    }
    
    @Override
    public void mouseMoved(MouseEvent e) {
         Optional input = controller.inputEvent(e.getID());
         controller.canDoAction(input);

    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    @Override
    public void mouseDragged(MouseEvent e) {
    }
    
    
    
    

}
