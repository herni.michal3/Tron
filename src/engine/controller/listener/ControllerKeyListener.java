package engine.controller.listener;

import engine.controller.BasicController;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Optional;

/**
 *
 * @author Michal
 */
public class ControllerKeyListener extends ControllerListener implements KeyListener {
    
    BasicController controller;

    public ControllerKeyListener(BasicController controller) {
        this.controller = controller;
    }
    

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        Optional input = controller.inputEvent(e.getKeyCode());
        controller.canDoAction(input);
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }


    
    
    
    

}
