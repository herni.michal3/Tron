package engine.model;

import java.awt.event.MouseEvent;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Michal
 */
public enum DefaultMouseMapping implements DefaultMapping{

    ONE;

    private static Map<Integer, ControllerAction> mapping;

    @Override
    public Map getMapping() {
        return initializeMapping(ordinal());
    }

    private Map initializeMapping(int ordinal) {
        mapping = new HashMap<>();
        switch (ordinal) {
            case 0: {
                mapping.put(MouseEvent.BUTTON1, Direction.LEFT);
                mapping.put(MouseEvent.BUTTON3, Direction.RIGHT);
                mapping.put(MouseEvent.MOUSE_MOVED, Direction.DOWN);
                mapping.put(MouseEvent.MOUSE_WHEEL, Direction.UP);
            }
            break;
        }
        return mapping;
    }

}
