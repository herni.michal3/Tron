package engine.model;

import engine.ScreenDevice;

/** 
 *
 * @author Michal
 */
public class Screen {

    private ScreenMode mode;
    private ScreenDevice device;
    private Bounds bounds;

    public Bounds getBounds() {
        return bounds;
    }

    public void setBounds(Bounds bounds) {
        this.bounds = bounds;
    }

    public void setDevice(ScreenDevice device) {
        this.device = device;
    }

    public ScreenDevice getDevice() {
        return device;
    }

    public Screen(ScreenMode mode, ScreenDevice device) {
        this.mode = mode;
        this.device = device;        
    }

    //TODO NOT YET FULLY IMPLEMENTED
    public enum ScreenMode {

        FULLSCREEN,
        WINDOW
    }

    //TODO NOT YET FULLY IMPLEMENTED
    public enum DisplayModes {

        DisplayMode1(1680, 1050, 32, 0),
        DisplayMode2(800, 600, 32, 0),
        DisplayMode3(800, 600, 24, 0),
        DisplayMode4(800, 600, 16, 0),
        DisplayMode5(640, 480, 32, 0),
        DisplayMode6(640, 480, 24, 0),
        DisplayMode7(640, 480, 16, 0);

        private DisplayModes(int x, int y, int z, int v) {
            
        }

    }

}
