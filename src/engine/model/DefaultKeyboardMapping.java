package engine.model;

import java.awt.event.KeyEvent;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Michal
 */
public enum DefaultKeyboardMapping implements DefaultMapping{

    ONE,
    TWO;

    private static Map<Integer, ControllerAction> mapping;

    @Override
    public Map getMapping() {
        return initializeMapping(ordinal());
    }

    private Map initializeMapping(int ordinal) {
        mapping = new HashMap<>();
        switch (ordinal) {
            case 0: {
                mapping.put(KeyEvent.VK_UP, Direction.UP);
                mapping.put(KeyEvent.VK_DOWN, Direction.DOWN);
                mapping.put(KeyEvent.VK_LEFT, Direction.LEFT);
                mapping.put(KeyEvent.VK_RIGHT, Direction.RIGHT);
            }
            break;
            case 1: {
                mapping.put(KeyEvent.VK_W, Direction.UP);
                mapping.put(KeyEvent.VK_S, Direction.DOWN);
                mapping.put(KeyEvent.VK_A, Direction.LEFT);
                mapping.put(KeyEvent.VK_D, Direction.RIGHT);
            }
            break;

        }
        return mapping;
    }

}
