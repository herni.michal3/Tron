package engine.model;

/**
 *
 * @author Michal
 */
public interface ControllerAction {
    
    void run();

}
