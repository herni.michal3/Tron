package engine.model;

import java.awt.Color;
import java.awt.Point;

/**
 *
 * @author Michal
 */
public abstract class Player extends GameObject{

    public Player(Point location, Direction direction, Color color, Bounds bounds) {
        super(location, direction, color, bounds);
        setType(DefautObjectType.PLAYER);
    }

}
