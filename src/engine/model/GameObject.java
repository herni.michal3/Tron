package engine.model;

import java.awt.Color;
import java.awt.Point;

/**
 *
 * @author Michal
 */
public class GameObject {
    protected Color color;
    protected Direction direction;
    protected Point location;
    protected Bounds bounds;
    protected int size = 10;    
    protected int moveStep = 5;
    protected boolean isMoving = false;
    protected GameObjectType type = DefautObjectType.STATIC;

     
    public GameObject(Point location, Direction direction, Color color, Bounds bounds) {
        this.location = location;
        this.direction = direction;
        this.color = color;
    }
    
    public GameObjectType getType() {
        return type;
    }

    public void setType(GameObjectType type) {
        this.type = type;
    }
    
    public void setSize(int size) {
        this.size = size;
    }

    public void setMoveStep(int step) {
        this.moveStep = step;
    }

    public Bounds getBounds() {
        return bounds;
    }

    protected void setBounds(Bounds bounds) {
        this.bounds = bounds;
    }
        
    public Color getColor() {
        return color;
    }

    public Point getLocation() {
        return location;
    }
    
    public void setLocation(Point point) {
        this.location = new Point(point);
    }

    public int getSize() {
        return size;
    }

    public int getMoveStep() {
        return moveStep;
    }

    public boolean isIsMoving() {
        return isMoving;
    }

    public void setIsMoving(boolean isMoving) {
        this.isMoving = isMoving;
    }

    public Point getBodySegment(int i) {
        return location;
    }

    public int getLength() {
        return 1;
    }

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

}
