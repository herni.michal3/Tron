
package engine.model;

/**
 *
 * @author Michal
 */
public enum DefautObjectType implements GameObjectType {
    
    PLAYER,
    STATIC,
    MOVING,
    WALL
    
}
