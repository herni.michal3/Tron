package engine.model;

import java.awt.Point;

/**
 * Screen bounding box: start point (0-lowerbound) to end point (max-upperbound)
 *
 * @author Michal
 */
public class Bounds {

    private Point start;
    private Point end;

    public Bounds(Point start, Point end) {

        if (start.distance(end) < 0) {
            throw new RuntimeException("Starting point must be smaller than endpoint");
        }

        this.start = start;
        this.end = end;

    }

    public Point getStart() {
        return start;
    }

    public int getStartX() {
        return start.x;
    }

    public int getStartY() {
        return start.y;
    }

    public void setStart(Point start) {
        this.start = start;
    }

    public Point getEnd() {
        return end;
    }

    public int getEndX() {
        return end.x;
    }

    public int getEndY() {
        return end.y;
    }

    public void setEnd(Point end) {
        this.end = end;
    }

}
