package engine.model;

import java.awt.Point;

/**
 * Default mapping UP 0, RIGHT 1, DOWN 2, LEFT 3 <br>
 * Controls are mapped elswhere, these are hardwired world coordinates
 * @author Michal
 */
public enum Direction implements ControllerAction {

    UP,
    RIGHT,
    DOWN,
    LEFT;

    public Point getVector() {
        switch (ordinal()) {
            case 0:
                return new Point(0, -1);
            case 1:
                return new Point(1, 0);
            case 2:
                return new Point(0, 1);
            case 3:
                return new Point(-1, 0);
        }
        return null;

    }
    
    public Direction turnRight(){
        return values()[(ordinal()+1) % values().length];
    }
    
    public Direction turnLeft(){
        return values()[(ordinal()+values().length-1) % values().length];
    }
    
    
    @Override
    public void run() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
